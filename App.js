import React, { useEffect } from 'react';
import { LogBox, StatusBar, View, Platform, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';

import Navigator from './src/navigator';

LogBox.ignoreAllLogs(true);

const App = () => {
    useEffect(() => {
        setTimeout(() => {
        SplashScreen.hide();
        }, 1000);
    }, []);

    return (
        <View style={styles.container}>
            <StatusBar
                animated={true}
                backgroundColor="#000"
                barStyle="light-content"
                showHideTransition="fade"
            />
            <NavigationContainer>
                <Navigator />
            </NavigationContainer>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        paddingTop: Platform.OS === 'ios' ? 60 : 0,
    },
});

export default App;
