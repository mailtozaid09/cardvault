export const media = {
    card_membership_fill: require('../assets/icons/tabbar/card_membership_fill.png'),
    fingerprint_fill: require('../assets/icons/tabbar/fingerprint_fill.png'),
    flight_fill: require('../assets/icons/tabbar/flight_fill.png'),
    health_fill: require('../assets/icons/tabbar/health_fill.png'),
    history_fill: require('../assets/icons/tabbar/history_fill.png'),
    water_drop_fill: require('../assets/icons/tabbar/water_drop_fill.png'),

    card_membership: require('../assets/icons/tabbar/card_membership.png'),
    fingerprint: require('../assets/icons/tabbar/fingerprint.png'),
    flight: require('../assets/icons/tabbar/flight.png'),
    health: require('../assets/icons/tabbar/health.png'),
    history: require('../assets/icons/tabbar/history.png'),
    water_drop: require('../assets/icons/tabbar/water_drop.png'),

    dummy_avatar: require('../assets/images/dummy_avatar.png'),

    lightbulb: require('../assets/icons/lightbulb.png'),
    visa: require('../assets/icons/visa.png'),
    chip: require('../assets/icons/chip.png'),
    wifi: require('../assets/icons/wifi.png'),



    card_1: require('../assets/images/cards/card_1.png'),
    card_2: require('../assets/images/cards/card_2.png'),
    card_3: require('../assets/images/cards/card_3.png'),
    card_4: require('../assets/images/cards/card_4.png'),
    card_5: require('../assets/images/cards/card_5.png'),
    card_6: require('../assets/images/cards/card_6.png'),
    card_7: require('../assets/images/cards/card_7.png'),
    card_8: require('../assets/images/cards/card_8.png'),
    card_9: require('../assets/images/cards/card_9.png'),
    card_10: require('../assets/images/cards/card_10.png'),
}
