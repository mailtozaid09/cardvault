import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { STRING_CONSTANTS, colors, fonts } from '../../../utils'
import AppText from '../../../components/text'

const CardMembershipScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
           
            <View style={styles.mainContainer} >
                <AppText
                    text={'CardMembershipScreen'}
                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                    customStyle={{color: colors.white}}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark_gray,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
})


export default CardMembershipScreen