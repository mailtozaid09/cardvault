import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { STRING_CONSTANTS, colors, fonts } from '../../../utils'
import AppText from '../../../components/text'
import { HomeHeader } from '../../../components/headers'
import { HomeTabbar } from '../../../navigator/tabbar'
import { FloatingActionButton } from '../../../components/button'

const HomeScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <HomeHeader title={"Home"} />
            <View style={styles.mainContainer} >

                <View style={{padding: 20, backgroundColor: colors.black}} >
                    <AppText
                        text={'All your credit cards'}
                        type={STRING_CONSTANTS.textConstants.HEADLINE1}
                        customStyle={{ fontSize: 30, color: colors.white, fontFamily: fonts.secondary4_bold_font, fontWeight: 'bold' }}
                    />
                    <AppText
                        text={'Find all your credit cards here'}
                        type={STRING_CONSTANTS.textConstants.HEADLINE1}
                        customStyle={{ fontSize: 16, marginTop: 6, color: colors.white, fontFamily: fonts.secondary4_regular_font }}
                    />
                </View>

                <HomeTabbar/>
            </View>

            <FloatingActionButton onPress={() => { }} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        // padding: 20,
        //alignItems: 'center',
        //justifyContent: 'center'
    }
})


export default HomeScreen