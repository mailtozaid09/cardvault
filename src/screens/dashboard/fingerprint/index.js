import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { STRING_CONSTANTS, colors, fonts } from '../../../utils'
import AppText from '../../../components/text'
import SwipeableStack from '../../../components/swipe'

const FingerprintScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <SwipeableStack />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        backgroundColor: colors.dark_gray,
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
})


export default FingerprintScreen