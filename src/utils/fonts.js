
const primary_font = 'Poppins'

const secondary1_font = 'BricolageGrotesque'
const secondary2_font = 'PulpDisplay'
const secondary3_font = 'Roboto'
const secondary4_font = 'Helvetica-65-Medium'

export default fonts =  {
    primary_bold_font: `${primary_font}-Bold`,
    primary_semi_bold_font: `${primary_font}-SemiBold`, 
    primary_medium_font: `${primary_font}-Medium`,
    primary_regular_font: `${primary_font}-Regular`,
    primary_light_font: `${primary_font}-Light`,
    primary_thin_font: `${primary_font}-Thin`,

    secondary1_bold_font: `${secondary1_font}-Bold`,
    secondary1_semi_bold_font: `${secondary1_font}-SemiBold`, 
    secondary1_medium_font: `${secondary1_font}-Medium`,
    secondary1_regular_font: `${secondary1_font}-Regular`,
    secondary1_light_font: `${secondary1_font}-Light`,

    secondary2_bold_font: `${secondary2_font}-Bold`,
    secondary2_semi_bold_font: `${secondary2_font}-SemiBold`, 
    secondary2_medium_font: `${secondary2_font}-Medium`,
    secondary2_regular_font: `${secondary2_font}-Regular`,
    secondary2_light_font: `${secondary2_font}-Light`,


    secondary3_bold_font: `${secondary3_font}-Bold`,
    secondary3_semi_bold_font: `${secondary3_font}-SemiBold`, 
    secondary3_medium_font: `${secondary3_font}-Medium`,
    secondary3_regular_font: `${secondary3_font}-Regular`,
    secondary3_light_font: `${secondary3_font}-Light`,

    secondary4_bold_font: `${secondary4_font}-Bold`,
    secondary4_regular_font: `${secondary4_font}-Regular`, 
}
