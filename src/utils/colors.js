export default colors = {
    primary: '#000000',

    
    black: '#000000',
    white: '#fff',

    dark_gray: '#252525',



    bg_color: '#fbfbfb',
    



    gray: '#696969',
    light_gray: '#ebecf0',
   
    disabled: '#C2C2C2',

    green: '#007c02',
    yellow: '#ffb32d',

    brown: '#EA9F5F',
    red: '#FF0000',

    cream: '#E8B88D',
    light_cream: '#F0E3D5',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    

    
    
    
    blue: '#81b3f3',

    orange: '#f7c191',
    reddish: '#ED4545',

}