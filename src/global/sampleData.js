import { media } from "../../assets/media";

export const credit_cards = [
    { card_image: media.card_1, card_number: '1234 5678 9101 1121', card_holder_name: 'John Doe', expiry_date: '12/24' },
    { card_image: media.card_2, card_number: '2345 6789 0123 4455', card_holder_name: 'Jane Doe', expiry_date: '05/25' },
    { card_image: media.card_3, card_number: '3456 7890 1234 5566', card_holder_name: 'Alice Smith', expiry_date: '09/23' },
    { card_image: media.card_4, card_number: '4567 8901 2345 6677', card_holder_name: 'Bob Johnson', expiry_date: '03/22' },
    { card_image: media.card_5, card_number: '5678 9012 3456 7788', card_holder_name: 'Emily Brown', expiry_date: '11/25' },
    { card_image: media.card_6, card_number: '6789 0123 4567 8899', card_holder_name: 'Michael Wilson', expiry_date: '07/24' },
    { card_image: media.card_7, card_number: '7890 1234 5678 9900', card_holder_name: 'Sarah Anderson', expiry_date: '02/23' },
    { card_image: media.card_8, card_number: '8901 2345 6789 0011', card_holder_name: 'David Martinez', expiry_date: '08/22' },
    { card_image: media.card_9, card_number: '9012 3456 7890 1122', card_holder_name: 'Olivia Garcia', expiry_date: '04/25' },
    { card_image: media.card_10, card_number: '0123 4567 8901 2233', card_holder_name: 'James Lee', expiry_date: '10/23' },
];
  