import React from 'react';
import { Image, View } from 'react-native';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { media } from '../../../assets/media';
import { colors } from '../../utils';

import { CardMembershipScreen, FingerprintScreen, FlightScreen, HealthScreen, HistoryScreen, WaterdropScreen } from '../../screens/dashboard';



const Tab = createMaterialTopTabNavigator();

export const HomeTabbar = () => {
  const screenOptions = ({ route }) => ({
        
    tabBarActiveTintColor: colors.primary,
    tabBarInactiveTintColor: 'gray',
    tabBarShowLabel: false, 
    swipeEnabled: false, 
    tabBarIcon: ({ color, focused }) => {
      let iconName;
      let iconSize = focused ? { height: 30, width: 30 } : { height: 28, width: 28 };

      if (route.name === 'Fingerprint') {
        iconName = focused ? media.fingerprint_fill : media.fingerprint 
      } else if (route.name === 'Flight') {
        iconName = focused ? media.flight_fill : media.flight 
      } else if (route.name === 'Waterdrop') {
        iconName = focused ? media.water_drop_fill : media.water_drop 
      } else if (route.name === 'Health') {
        iconName = focused ? media.health_fill : media.health 
      } else if (route.name === 'History') {
        iconName = focused ? media.history_fill : media.history 
      } else if (route.name === 'CardMembership') {
        iconName = focused ? media.card_membership_fill : media.card_membership 
      }

      return (
        <View style={{height: 40, width: 40, alignItems: 'center', justifyContent: 'center', }}>
          <Image source={iconName} style={[iconSize, { resizeMode: 'contain' }]} />
        </View>
      );
    },
    
    tabBarStyle: {
        
      backgroundColor: colors.dark_gray
    },
    tabBarIndicatorStyle: {
      backgroundColor: colors.dark_gray
    },
  });

  return (
    <Tab.Navigator 
        screenOptions={screenOptions} 
       
    >
        <Tab.Screen 
            name="Fingerprint" 
            component={FingerprintScreen} 
        />
        <Tab.Screen 
            name="Flight" 
            component={FlightScreen} 
        />
        <Tab.Screen 
            name="Waterdrop" 
            component={WaterdropScreen} 
        />
        <Tab.Screen 
            name="Health" 
            component={HealthScreen} 
        />
        <Tab.Screen 
            name="History" 
            component={HistoryScreen} 
        />
        <Tab.Screen 
            name="CardMembership" 
            component={CardMembershipScreen} 
        />
    </Tab.Navigator>
  );
};
