import React, {useState, useEffect, useContext} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/dashboard/home';


const Stack = createStackNavigator();

const Navigator = ({}) => {

    useEffect(() => {
        console.log("Navigator > > ");
    }, [])

    return (
        <>
            <Stack.Navigator 
            initialRouteName={'HomeScreen'}  
            >
                <Stack.Screen
                    name="HomeScreen"
                    component={HomeScreen}
                    options={{
                        headerShown: false
                    }}
                />
            </Stack.Navigator>
        </>
    );
}

export default Navigator