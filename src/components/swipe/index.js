import React, { useState } from 'react';
import { StyleSheet, View, Text, Dimensions, Image } from 'react-native';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import { credit_cards } from '../../global/sampleData';
import { media } from '../../../assets/media';
import AppText from '../text';
import { STRING_CONSTANTS, colors, fonts } from '../../utils';
import { PrimaryButton } from '../button';

const { width } = Dimensions.get('window');
const CARD_WIDTH = width - 40; 
const CARD_HEIGHT = width - 150; 

const CreditCard = ({ data, index, onSwipeLeft, onSwipeRight }) => {
  const cardStyle = {
    ...styles.card,
    zIndex: index,
    transform: [{ translateY: index * 40 }], 
  };

  return (
    <GestureRecognizer
      key={index}
      onSwipeLeft={onSwipeLeft}
      onSwipeRight={onSwipeRight}
      config={{ velocityThreshold: 0.3, directionalOffsetThreshold: 80 }}
      style={cardStyle}
    >
        <View>
            <Image source={data.card_image} style={{height: CARD_HEIGHT, width: CARD_WIDTH, borderRadius: 8, }} />
       
            <View style={{position: 'absolute', width: '100%', height: '100%', justifyContent: 'space-between'}} >
                <View style={styles.topContainer} >
                    <AppText
                        text={'Bank of Designers'}
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{fontSize: 14, fontFamily: fonts.secondary1_bold_font, fontWeight: 700,  color: colors.white}}
                    />
                    <View style={styles.chipContainer} >
                        <Image source={media.chip} style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                        <Image source={media.wifi} style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                    </View>
                    <AppText
                        text={data.card_number}
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{fontSize: 24, fontFamily: fonts.secondary1_bold_font, fontWeight: 700,  color: colors.white}}
                    />
                </View>
                <View style={styles.bottomContainer} >
                    <View style={{flex: 1, }} >
                        <AppText
                            text={'Card holder name'}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{fontSize: 12, fontFamily: fonts.secondary1_bold_font, fontWeight: 700,  color: colors.white}}
                        />
                        <AppText
                            text={data.card_holder_name}
                            numberOfLines={1}
                            type={STRING_CONSTANTS.textConstants.HEADLINE1}
                            customStyle={{fontSize: 20, fontFamily: fonts.secondary1_bold_font, fontWeight: 700, color: colors.white}}
                        />
                    </View>
                    <View style={{width: 100, marginLeft: 10}} >
                        <AppText
                            text={'Expiry date'}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{fontSize: 12, fontFamily: fonts.secondary1_bold_font, fontWeight: 700,  color: colors.white}}
                        />
                        <AppText
                            text={data.expiry_date}
                            type={STRING_CONSTANTS.textConstants.HEADLINE1}
                            customStyle={{fontSize: 20, fontFamily: fonts.secondary1_bold_font, fontWeight: 700, color: colors.white}}
                        />
                    </View>
                    <Image source={media.visa} style={{ height: 40, width: 40, resizeMode: 'contain' }} />
                </View>
            </View>
        </View>
    </GestureRecognizer>
  );
};

const SwipeableStack = () => {
    const [cards, setCards] = useState(credit_cards); 

    const onSwipeLeft = (index) => {
        setCards((prevCards) => prevCards.filter((_, i) => i !== index));
    };

    const onSwipeRight = (index) => {
        setCards((prevCards) => prevCards.filter((_, i) => i !== index));
    };

    return (
        <View style={styles.container}>
            {cards?.length > 0 ? (
                <>
                    {cards.map((item, index) => (
                        <CreditCard
                            key={item}
                            index={index}
                            data={item}
                            onSwipeLeft={() => onSwipeLeft(index)}
                            onSwipeRight={() => onSwipeRight(index)}
                        />
                    ))}
                </>
            ):(
                <View style={styles.emptyContainer} >
                    <AppText
                        text={'No Cards Left'}
                        type={STRING_CONSTANTS.textConstants.HEADLINE1}
                        customStyle={{fontSize: 20, marginVertical: 20, fontFamily: fonts.secondary1_bold_font, fontWeight: 700, color: colors.white}}
                    />
                    <PrimaryButton
                        title={"Get Cards"}
                        onPress={() => { setCards(credit_cards)}}
                    />
                </View>
            )}
        
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.dark_gray,
    },
    cardContainer: {
        flex: 1,
        width: '100%',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    card: {
        position: 'absolute',
        width: CARD_WIDTH,
        height: CARD_HEIGHT,
        borderRadius: 8,
        backgroundColor: '#ffffff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 4,
        elevation: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    bottomContainer: { 
        paddingHorizontal: 15, 
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#00000050',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
    },
    topContainer: {
        flex: 1, 
        paddingHorizontal: 15, 
        paddingVertical: 10,
        justifyContent: 'space-between',
    },
    chipContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    emptyContainer: {
        marginTop: 20, 
        alignItems: 'center', 
        flex: 1,
        width: width-40, 
        paddingHorizontal: 20,
    },
});

export default SwipeableStack;
