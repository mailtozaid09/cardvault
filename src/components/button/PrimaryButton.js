import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { STRING_CONSTANTS, colors } from '../../utils'
import CustomIcon from '../CustomIcon'
import AppText from '../text'

const PrimaryButton = ({ onPress, title}) => {

    const navigation = useNavigation()

    return (
        <>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}
                style={styles.container}
            >
                <AppText
                    text={title}
                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                    customStyle={{color: colors.black, marginBottom: 2}}
                />
            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        width: '100%',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
    },
})

export default PrimaryButton
