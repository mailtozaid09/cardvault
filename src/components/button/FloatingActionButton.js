import React from 'react';
import { TouchableOpacity, Image, StyleSheet, View } from 'react-native';
import { colors } from '../../utils';
import CustomIcon from '../CustomIcon';

const FloatingActionButton = ({ onPress }) => {
    return (
        <TouchableOpacity style={styles.fab} onPress={onPress}>
            <CustomIcon
                iconName="plus"
                iconType="AntDesign"
                iconColor={colors.black}
                iconContainer={{}}
            />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    fab: {
        position: 'absolute',
        bottom: 20,
        right: 20,
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 8,
        shadowColor: '#000', 
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    fabIcon: {
        width: 24,
        height: 24,
        tintColor: colors.white,
    },
});

export default FloatingActionButton;
