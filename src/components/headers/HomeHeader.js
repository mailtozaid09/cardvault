import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { BackButton } from '../button'
import AppText from '../text'
import { STRING_CONSTANTS, colors, fonts } from '../../utils'
import CustomIcon from '../CustomIcon'
import { useNavigation } from '@react-navigation/native'
import { media } from '../../../assets/media'
import LinearGradient from 'react-native-linear-gradient'

const HomeHeader = ({title, headerMsg, onPress, }) => {

    const navigation = useNavigation()


    return (
        <View style={styles.container} >
                
            <TouchableOpacity
                activeOpacity={0.5} 
                onPress={() => {}}
            >
                <LinearGradient 
                    start={{x: 0, y: 0.5}} 
                    end={{x: 1, y: 0.25}}
                    colors={[ '#fb83a5','#f2d134', ]} 
                    style={styles.avatarContainer} 
                />
            </TouchableOpacity>
          
            <TouchableOpacity
                activeOpacity={0.5} 
                onPress={() => {}}
                style={styles.iconContainer} 
            >
                <Image source={media.lightbulb} style={styles.bulbStyle} />
                <AppText
                    text={'Tips'}
                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                    customStyle={{color: colors.white, fontSize: 20, fontFamily: fonts.primary_regular_font, fontWeight: '500' }}
                />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 70,
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 14,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.black
    },
    iconContainer: {
        height: 52,
        width: 99,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 12,
        borderRadius: 5,
        justifyContent: 'space-between',
        backgroundColor: colors.dark_gray,
    },
    bulbStyle: {
        height: 30, 
        width: 30,
    },
    avatarContainer: {
        height: 60,
        width: 60,
        borderRadius: 35,
        marginRight: 10
    }
})

export default HomeHeader